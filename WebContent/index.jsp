<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Health Insurance</title>
</head>
<body>
<style>
body {
    background-color: lightblue;
}
</style>

<div>
<h1 style="color:blue;">Health Insurance Premium Generator</h1>
<form action="Control" method="post">

	<h4 style="color:blue; font-size: 150%;">Name: <input type="text" name="name" style="width:200px;height:30px;font-size: 75%;" value="" maxlength="50" required /></h4>
	
	<h4 style="color:blue; font-size: 150%;">Gender: <select style="width:200px;height:30px;font-size: 75%;" name="gender" required>
				<option value="M">Male</option>
				<option value="F">Female</option>
				<option value="O">Other</option>
		</select></h4>
	<h4 style="color:blue; font-size: 150%;"> Age:&nbsp;&nbsp;<select style="width:200px;height:30px;font-size: 75%;" name="personAge" required>
				<option value="1">less than 18</option>
				<option value="2">18-25</option>
				<option value="3">25-30</option>
				<option value="4">30-35</option>
				<option value="5">35-40</option>
				<option value="6">greater than 40</option>
		</select></h4>
		
		<h2 style="color:orange; font-size: 150%;">Current Health Condition</h2>
		
		<h4 style="color:blue; font-size: 150%;">Hypertension: <select style="width:200px;height:30px;font-size: 75%;" name="hypertension" required>
				<option style="color:blue; font-size: 150% value="Y" class="new" >Yes</option>
				<option value="N">No</option></select></h4>
				
				
		<h4 style="color:blue; font-size: 150%;">Blood pressure: <select style="width:200px;height:30px;font-size: 75%;" name="bloodpressure" required>
				<option value="Y">Yes</option>
				<option value="N">No</option></select></h4>
				
		<h4 style="color:blue; font-size: 150%;">Blood sugar: <select style="width:200px;height:30px;font-size: 75%;" name="bloodsugar" required>
				<option value="Y">Yes</option>
				<option value="N">No</option></select></h4>
				
		<h4 style="color:blue; font-size: 150%;">Overweight: <select style="width:200px;height:30px;font-size: 75%;" name="overweight" required>
				<option value="Y">Yes</option>
				<option value="N">No</option></select></h4>
		
		
		
	<h2 style="color:orange; font-size: 150%;">Habits</h2>
		
		<h4 style="color:blue; font-size: 150%;">Smoking: <select style="width:200px;height:30px;font-size: 75%;" name="smoking" required>
				<option value="Y">Yes</option>
				<option value="N">No</option></select></h4>
				
		<h4 style="color:blue; font-size: 150%;">Alcohol: <select style="width:200px;height:30px;font-size: 75%;"  name="alcohol" required>
				<option value="Y">Yes</option>
				<option value="N">No</option></select></h4>
		<h4 style="color:blue; font-size: 150%;">Daily exercise: <select style="width:200px;height:30px;font-size: 75%;" name="dailyexercise" required>
				<option value="Y">Yes</option>
				<option value="N">No</option></select></h4>
				
		<h4 style="color:blue; font-size: 150%;">Drugs: <select style="width:200px;height:30px;font-size: 75%;" name="drugs" required>
				<option value="Y">Yes</option>
				<option value="N">No</option></select></h4>
		
		
	<button name="submit" align="middle" style="width:250px;height:60px; color:blue;font-size: 150%">Submit</button>
	
	

</form>
	
</body>
</html>