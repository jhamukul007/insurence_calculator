package com.control.demo;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Control
 */
@WebServlet("/Control")
public class Control extends HttpServlet {
	float fee = 0f;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String name = request.getParameter("name");
		String gen = request.getParameter("gender");
		String age = request.getParameter("personAge");
		String hyper = request.getParameter("hypertension");
		String b_pre = request.getParameter("bloodpressure");
		String b_suger = request.getParameter("bloodsugar");
		String o_weight = request.getParameter("overweight");
		String smoke = request.getParameter("smoking");
		String alcohol = request.getParameter("alcohol");
		String dailyexercise = request.getParameter("dailyexercise");
		String drug = request.getParameter("drugs");
		String s = "";

		PrintWriter out = response.getWriter();
		// out.print(name+" "+gen+" "+age+" "+hyper+" "+b_pre+" "+b_suger+" "+o_weight+"
		// "+smoke+" "+alcohol+" "+dailyexercise+" "+drug);

		Map<String, Float> age_fee = new LinkedHashMap<String, Float>();
		age_fee.put("1", 5000f);
		age_fee.put("2", 5500f);
		age_fee.put("3", 6050f);
		age_fee.put("4", 6655f);
		age_fee.put("5", 7320.5f);
		age_fee.put("6", 8784.6f);
		out.println("<br/>" + "Name: Mr. " + name+"<br/>" );
		out.println();
		fee = age_fee.get(age);

		if (gen.equals("M")) {
			fee = (float) (fee + fee * 0.02);
		out.println("Gender: Male"+"<br/>" );
			
		}

		if (hyper.equals("Y")) 
		{
			fee = (float) (fee * 0.01) + fee;
			 out.println("Hypertension: Yes"+"<br/>" );
		} else if (hyper.equals("N")) {
			 out.println("Hypertension: No"+"<br/>" );
		}

		if (b_pre.equals("Y")) {
			fee = (float) (fee * 0.01) + fee;
			out.println("<br/>" + "Blook Pressure: Yes"+"<br/>" );
		} else if (b_pre.equals( "N")) {
			out.println("Blook Pressure: No"+"<br/>" );
		}

		if (b_suger.equals("Y")) {
			fee = (float) (fee * 0.01) + fee;
			out.println("Blood Suger: Yes"+"<br/>" );
		} else if (b_suger.equals("N")) {
			out.println("Blood Suger: No"+"<br/>" );
		}

		if (o_weight.equals("Y")) {
			fee = (float) (fee * 0.01) + fee;
			out.println("Overweight: Yes"+"<br/>" );
		} else if (o_weight.equals("N")) {
			out.println("Overweight: No"+"<br/>" );
		}

		if (dailyexercise.equals("Y")) {
			fee = (float) (fee - (fee * 0.03));
			out.println("Daily Exercise: Yes"+"<br/>" );
		} else if (dailyexercise.equals("N")) {
			out.println("Daily Exercise: No"+"<br/>" );
		}

		if (smoke.equals("Y")) {

			fee = (float) (fee * 0.03) + fee;
			out.println("Smoking: Yes"+"<br/>" );
		} 
		else if (smoke.equals("N")) {
			out.println("Smoking: Yes"+"<br/>" );
		}

		if (alcohol.equals("Y")) {
			fee = (float) (fee * 0.03) + fee;
			out.println("Consumption of Alcohol: Yes"+"<br/>" );
		}

		else if (alcohol.equals("N")) {
			out.println("Consumption of Alcohol: No"+"<br/>" );
		}

		if (drug.equals("Y")) {
			fee = (float) (fee * 0.03) + fee;
			out.println("Drugs: Yes"+"<br/>" );
		} else if (drug.equals("N")) {
			out.println("Drugs: No"+"<br/>" );
		}

		out.println("------------------------------------------------------------");

		out.println("<br/>" +"<br/>" +"Total Amount to pay: " + Math.ceil(fee));

	}

}
